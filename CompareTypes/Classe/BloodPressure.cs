﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareTypes.Classe
{
    public class BloodPressureClass
    {
        private int sistolico;
        private int diastolico;

        public BloodPressureClass() { }

        public BloodPressureClass(int sistolico, int diastolico)
        {
            Sistolico = sistolico;
            Diastolico = diastolico;
        }

        public int Sistolico
        {
            get { return sistolico; }
            set { sistolico = value; }
        }

        public int Diastolico
        {
            get { return diastolico; }
            set { diastolico = value; }
        }

        /// <summary>
        /// Sobrescrevendo o método ToSTring.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} x {1}", this.Sistolico, this.Diastolico);
        }
        
        /// <summary>
        /// Sobrescrevendo o metodo GetHashCode
        /// mais informações em: http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Sistolico.GetHashCode();
                hash = hash * 23 + Diastolico.GetHashCode();
                return hash;
            }
        }

        /// <summary>
        /// Sobrescrevendo o método Equals.
        /// http://stackoverflow.com/questions/371328/why-is-it-important-to-override-gethashcode-when-equals-method-is-overridden
        /// http://stackoverflow.com/questions/638761/gethashcode-override-of-object-containing-generic-array
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            BloodPressureClass other = obj as BloodPressureClass;

            // check for null
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            // Check for same reference
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            // check for same properties values
            return this.Sistolico == other.Sistolico
                && this.Diastolico == other.Diastolico;
             
        }
    }
}
