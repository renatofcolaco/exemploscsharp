﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareTypes.Estrutura
{
    public struct BloodPressureStruct
    {
        private int sistolico;
        private int diastolico;

        public BloodPressureStruct(int sistolico, int diastolico)
            : this()// http://stackoverflow.com/questions/2534960/struct-constructor-fields-must-be-fully-assigned-before-control-is-returned-to
        {
            Sistolico = sistolico;
            Diastolico = diastolico;

            // Boa prática é inicializar utilizando propriedades.
            // Dessa forma se você adicionar alguma restrição,
            // por exemplo, de tamanho você implementa isso apenas uma vez no set.
            // http://stackoverflow.com/questions/6019480/constructor-initialization-of-properties-in-c-sharp

        }

        // to do adicionar tratamento, ex: valores mínimos e máximos

        public int Sistolico
        {
            get { return sistolico; }
            set { sistolico = value; }
        }

        public int Diastolico
        {
            get { return diastolico; }
            set { diastolico = value; }
        }
    }
}
