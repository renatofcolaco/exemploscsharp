﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModificadoresAcesso
{
    public class Externa
    {
        public Externa(string nome)
        {
            Nome = nome;
        }

        protected internal string Nome { get; set; }

        /// <summary>
        /// Classe protegida e interna
        /// </summary>
        protected internal class Auxiliar
        {
            public Auxiliar() { }
            public string MyProperty { get; set; }
        }
    }
}
