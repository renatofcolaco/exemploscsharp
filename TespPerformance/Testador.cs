﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TespPerformance
{
    public class Testador
    {
        public TimeSpan TestaMetodo(Action method)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            method();
            sw.Stop();

            return sw.Elapsed;
        }
    }
}
