﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModificadoresAcesso;

namespace ModificadoresAcesso2
{
    public class Outra : Externa
    {
        public Outra(string nome)
            : base(nome)
        {
            // Iniciando Auxiliar dentro do construtor
            Aux = new Auxiliar();
        }

        // Acessando classe protected internal definida dentro de Externa
        private Auxiliar Aux { get; set; }


        /// <summary>
        /// Método que retorna propriedade MyProperty da classe Auxiliar
        /// </summary>
        /// <returns></returns>
        public string MetodoAux()
        {
            return Aux.MyProperty;
        }

        /// <summary>
        /// Método que retorna a propriedade nome, herdada de Externo
        /// </summary>
        /// <returns></returns>
        public string MetodoExt()
        {
            return Nome;
        }
    }
}
