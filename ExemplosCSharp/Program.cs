﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompareTypes.Classe;
using CompareTypes.Estrutura;
using TespPerformance;
using System.Collections;

namespace ExemplosCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcao;

            Console.WriteLine("*** Testando exemplos *** ");

            do
            {                
                Console.WriteLine("Qual exemplo você deseja testar?\n");

                Console.WriteLine("1 - Comparar Tipos");
                Console.WriteLine("2 - Validação");
                Console.WriteLine("3 - TestPerformance");
                Console.WriteLine("4 - TestPerformance: Boxing e Unboxing");

                Console.WriteLine("\n0 - Sair.");

                Console.Write("\n");

                string input = Console.ReadLine();
                
                // Validando entrada do usuário
                if(!int.TryParse(input, out opcao)){
                    opcao = -1;
                }

                Console.Write("\n");

                switch (opcao)
                {
                    case 1:
                        CompararTipos();
                        break;
                    case 2:                        
                        Validacao();
                        break;
                    case 3:
                        TestImplicitCast();
                        break;
                    case 4:
                        TestBoxingUnboxing();
                        break;
                }

                Console.Write("\n");

            } while (opcao != 0);

            Console.WriteLine("Saindo do programa! Pressione uma tecla para sair.");
            Console.ReadKey();
        }

        public static void CompararTipos()
        {
            Console.Write("CompararTipos");
            Console.WriteLine("Classes vs Estruturas\n");

            BloodPressureClass pressaoCl = new BloodPressureClass(120,90);
            
            Console.WriteLine("Sua pressão é: {0}\n", pressaoCl);

            //BloodPressureClass pressaoCl2 = pressaoCl;

            BloodPressureClass pressaoCl2 = new BloodPressureClass(90, 120);

            // Testando referências

            //pressaoCl = null;
            //BlackHole(pressaoCl);

            //if(pressaoCl == null)
            //{
            //    Console.WriteLine("O objeto pressão foi destruído pelo buraco negro.");
            //}
            //else
            //{
            //    Console.WriteLine("O objeto pressão continua vivo!");                
            //}

            // Testando Igualdades - compara referências

            Console.WriteLine("\nComparando com o operador ==\n");
            if (pressaoCl == pressaoCl2)
            {
                Console.WriteLine("pressaoCl é igual à pressaoCl2.\n");
            }
            else
            {
                Console.WriteLine("pressaoCl é diferente de pressaoCl2.\n");
            }

            Console.WriteLine("\nComparando com o método Equals\n");

            if (pressaoCl.Equals(pressaoCl2))
            {
                Console.WriteLine("pressaoCl é igual à pressaoCl2.\n");
            }
            else
            {
                Console.WriteLine("pressaoCl é diferente de pressaoCl2.\n");
            }
            
            Console.WriteLine("\nHashcodes\n");
            Console.WriteLine("pressaoCl: {0}", pressaoCl.GetHashCode());
            Console.WriteLine("pressaoCl2: {0}", pressaoCl2.GetHashCode());
            
            // Estrutura

            //BloodPressureStruct pressaoStru = new BloodPressureStruct();
            //pressaoStru.Sistolico = 130;
            //pressaoStru.Diastolico = 85;

            //BloodPressureStruct pressaoStru2 = new BloodPressureStruct(140, 80);

            //if (pressaoStru == pressaoStru2)
            //{
            //    Console.WriteLine("Pressão 2 é igual pressão 3");
            //}
            //else
            //{
            //    Console.WriteLine("Pressão 2 é diferente pressão 3");
            //}
        }

        public static void Validacao()
        {
            Console.WriteLine("Validação");
        }

        public static void TestImplicitCast()
        {
            // Instanciando a classe que irá executar os testes
            Testador testador = new Testador();

            // Chamando o método que executa os testes
            // Atenção! Observe que o método recebe outro método
            // e executa-o internamente.
            TimeSpan resultado = testador.TestaMetodo(testIntToLong);

            // Imprimindo o resultado
            Console.WriteLine("Resultado do teste de performance testIntToLong: ");
            Console.WriteLine("Tempo decorrido: {0}", resultado);

            // Executando novo teste para comparação
            // desta vez estou utilizando expressões lambdas
            TimeSpan resultado2 = testador.TestaMetodo(() => {

                int x = 10;
                int y = 0;

                for (long i = 0; i < 1000000000; i++)
                {
                    x = y;
                }
            });

            // Imprimindo o resultado
            Console.WriteLine("Resultado do teste de performance do método anônimo é: ");
            Console.WriteLine("Tempo decorrido: {0}", resultado2);
        }

        public static void TestBoxingUnboxing()
        {
            Console.WriteLine("Iniciando testes de boxing e unboxing: ");

            // Instanciando a classe que irá executar os testes
            Testador testador = new Testador();

            TimeSpan resultado = testador.TestaMetodo(testBoxing);
            Console.WriteLine("O tempo decorrido é de: {0}", resultado);

            Console.WriteLine("----------------------");

            // -----

            Console.WriteLine("Iniciando testes sem boxing usando generics: ");

            TimeSpan resultado2 = testador.TestaMetodo(testWithoutBoxing);
            Console.WriteLine("O tempo decorrido é de: {0}", resultado2);

            Console.WriteLine("\n");
        }

        public static void BlackHole(object obj)
        {
            obj = null;
        }

        #region métodos de teste

        public static void testIntToLong()
        {
            int x = 99;
            long y = 10;
            
            for (long i = 0; i < 1000000000; i++)
            {
                y = x;
            }
        }

        public static void testBoxing()
        {            
            Random rdn = new Random();
            ArrayList intList = new ArrayList();
            int size = 1000000;
            int[] intArray = new int[size];


            for (int i = 0; i < size; i++)
            {
                intList.Add(rdn.Next(1, 100));
            }

            int count = 0;
            foreach (object item in intList)
            {
                intArray[count] = (int)item;
                count++;
            }
        }

        public static void testWithoutBoxing()
        {
            Random rdn = new Random();
            List<int> intList = new List<int>();

            int size = 1000000;
            int[] intArray = new int[size];


            for (int i = 0; i < size; i++)
            {
                intList.Add(rdn.Next(1, 100));
            }

            int count = 0;
            foreach (int item in intList)
            {
                intArray[count] = item;
                count++;
            }
        }

        #endregion
    }
}
